﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

/*
 * Записиати математичну модель задачі ЛП та зазначити економічний зміст цільової функції і системи обмежень.
 * Написати програму розв'язування та з її допомогою знайти розв'язок(максимальне (мінімальне) значення функції 
 * та значення змінних, при якому воно досягаєтья) задачі ЛП згідно з варіантом з Додатка №1 до лабораторної роботи №1
 * 
 * TODO
 * -зведення до канонічної форми
 * -виведення усіх СТ
 * 2.Введення вхідних даних вручну:
 * -задати кількість рядків та стовпців
 * -задати цільову функцію
 * -задати коефіцієнти обмеження на змінні "типу >="
 * 3.Передбачити можливість некоректного введення даних
 * 4.Передбачити можливість покрокового відображення побудови симплекс-таблиць
 * 5.Підписання таблиць
 * 6.Виведення відповідного повідомлення у випадку не існування оптимального плану     
 */

namespace SimplexMethod
{
    public class Simplex
    {
        public int Columns { get; set; } = 0;
        public int Rows { get; set; } = 0;

        private double[,] simplexTable;

        private bool isUnbounded = false;
        public Simplex(int numberOfContraints, int numberOfUnknows)
        {
            Rows = numberOfContraints + 1;
            Columns = numberOfUnknows + 1;
            simplexTable = new double[Rows, Columns];
        }

        public void PrintTable()
        {
            for(int i = 0; i < Rows; ++i)
            {
                for(int j = 0; j < Columns; ++j)
                    Console.Write(simplexTable[i, j] );
                Console.WriteLine();
            }   
            Console.WriteLine();
        }

        public bool IsOptimal()
        {
            int count = 0;

            for (int i = 0; i < Columns - 1; ++i)
                if (simplexTable[Rows - 1, i] >= 0)
                    ++count;

            return count == Columns - 1 ? true : false;
        }

        public void FillTable(double[,] data) => simplexTable = data;

        public string Compute()
        {
            if (IsOptimal())
                return "Is optimal";

            int pivotColumn = FindPivotColumn();
            Console.WriteLine($"Pivot column: {pivotColumn}");

            double[] ratios = CalculateRatios(pivotColumn);
            if (isUnbounded == true)
                return "Is unbounded";

            int pivotRow = FindMinimumValue(ratios);

            FormNextTable(pivotRow, pivotColumn);

            return "Not optimal";
        }
        private int FindPivotColumn()
        {
            double[] lastRow = new double[Columns];
            int location = 0;
            int negativeSign = 0;

            for (int i = 0; i < Columns - 1; ++i)
                if (simplexTable[Rows - 1, i] < 0)
                    ++negativeSign;

            if (negativeSign > 0)
            {
                for (int i = 0; i < Columns; ++i)
                    lastRow[i] = Math.Abs(simplexTable[Rows - 1, i]);
                location = FindBiggestValue(lastRow);
            }
            else
                location = negativeSign - 1;

            return location;
        }
        private int FindBiggestValue(double[] lastRow)
        {
            double max = 0;
            int location = 0;

            for(int i = 0; i < lastRow.Length; ++i)
                if (lastRow[i] > max)
                {
                    max = lastRow[i];
                    location = i;
                }

            return location;
        }

        private int FindMinimumValue(double[] lastColumn)
        {
            int location = 0;
            double min = lastColumn[0];

            for(int i = 0; i < lastColumn.Length; ++i)
                if (lastColumn[i] > 0 && lastColumn[i] < min)
                {
                    min = lastColumn[i];
                    location = i;
                }

            return location;
        }

        private void FormNextTable(int pivotRow, int pivotColumn)
        {
            double pivotElement = simplexTable[pivotRow, pivotColumn];
            double[] pivotRowValues = new double[Columns];
            double[] pivotColumnValues = new double[Columns];
            double[] newRow = new double[Columns];

            for (int i = 0; i < Columns; ++i)
                pivotRowValues[i] = simplexTable[pivotRow, i];

           for(int i = 0; i < Rows; ++i)
                pivotColumnValues[i] = simplexTable[i, pivotColumn];

            for (int i = 0; i < Columns; ++i)
                newRow[i] = pivotRowValues[i] / pivotElement;

            for (int i = 0; i < Rows; ++i)
                if (i != pivotRow)
                    for (int j = 0; j < Columns; ++j)
                        simplexTable[i, j] = simplexTable[i, j] - (pivotColumnValues[i] * newRow[j]);

            for (int i = 0; i < Columns; ++i)
                simplexTable[pivotRow, i] = newRow[i];         
        }

        private double[] CalculateRatios(int column)
        {
            double[] positiveEnters = new double[Rows];
            double[] result = new double[Rows];
            int negativeCount = 0;

            for (int i = 0; i < Rows; ++i)
            {
                if (simplexTable[i, column] > 0)
                    positiveEnters[i] = simplexTable[i, column];
                else
                {
                    positiveEnters[i] = 0;
                    negativeCount++;
                }
            }

            if(negativeCount == Rows)
                isUnbounded = true;
            else
                for(int i = 0; i < Rows; ++i)
                    if (positiveEnters[i] > 0)
                        result[i] = simplexTable[i, Columns - 1] / positiveEnters[i];

            return result;
        }
    }
}
